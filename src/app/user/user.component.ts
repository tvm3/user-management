import { Component } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { User } from '../model/user';
import { UserService } from '../services/user.service';
import { MessageService } from 'primeng/api';
import { RouterTestingHarness } from '@angular/router/testing';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent {
  users: User[] = [];
  user!: User;
  productDialog: boolean = false;
  isNew!: boolean;
  hidden!: boolean;
  isDelete!: boolean;
  buttonLable!: string;

  constructor(
    private userService: UserService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.productDialog = false;
    this.isNew = true;
    this.hidden = true;
    this.isDelete = false;
    this.buttonLable = 'Create';
    this.userService.findAll().subscribe((data) => {
      this.users = data;
    });
  }

  editUser(currentUser: User) {
    this.user = currentUser;
    this.buttonLable = 'Update';
    this.productDialog = true;
    this.isNew = false;
  }

  delete(user: User) {
    this.isDelete = true;
    this.productDialog = true;
    this.user = user;
    this.buttonLable = 'Delete';
  }

  deleteUser() {
    this.hideDialog();
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + this.user.id + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.userService.delete(this.user).subscribe(
          () => {
            this.users.forEach((item, index) => {
              if (item.id === this.user.id) this.users.splice(index, 1);
            });
            this.messageService.add({
              severity: 'success',
              summary: 'Successful',
              detail: 'User Deleted',
              life: 3000,
            });
          },
          (error) => {
            console.log(error);
            //Error callback
            this.messageService.add({
              severity: 'error',
              detail: error.error.errorMessage,
              summary: 'Error',
              life: 3000,
            });
            //throw error;   //You can also throw the error to a global error handler
          }
        );
      },
    });
  }

  hideDialog() {
    this.productDialog = false;
  }

  onHide() {
    this.ngOnInit();
  }

  openNew() {
    this.isNew = true;
    this.user = new User();
    this.user.email = '';
    this.user.firstName = '';
    this.user.lastName = '';
    this.productDialog = true;
  }

  saveProduct() {
    if (this.isNew) {
      this.userService.save(this.user).subscribe({
        error: (error) => {
          //Error callback
          this.messageService.add({
            severity: 'error',
            detail: error,
            life: 3000,
          });
        },
        next: (data) => {
          this.isNew = false;
          this.users.push(data);
        },
      });
    } else {
      this.userService.update(this.user).subscribe((data) => {
        this.ngOnInit();
      });
    }
    this.productDialog = false;
  }
}
